from dataclasses import dataclass, field


class ParsingException(Exception):
    """
    General parsing error. Raised whenever parser cannot parse data correctly
    """
    pass


class IntegrityException(Exception):
    """
    Provided checksum does not match with actual checksum of the packet
    """
    pass


@dataclass
class ProtocolPacket:
    data: bytes

    def hash(self):
        # TODO: Use hash function from RFC
        return self.data[20:]


@dataclass
class Flags:
    value: int
    dont_fragment: bool = field(init=False)
    more_fragments: bool = field(init=False)

    def __post_init__(self):
        # TODO: update dont_fragment and more_fragments fields after hex_value is added
        self.dont_fragment = True if bin(self.value)[2] == "1" else False
        self.more_fragments = True if bin(self.value)[3] == "1" else False


@dataclass
class IPPacket:
    version: int
    ihl: int
    dscp: int
    ecn: int
    total_length: int
    identification: int
    flags: Flags
    fragment_offset: int
    ttl: int
    protocol: int
    header_checksum: int
    source_ip: int
    destination_ip: int
    data: ProtocolPacket

    @staticmethod
    def from_bytes(data: bytes) -> "IPPacket":
        # TODO: write your code, converting `data` byte array into IPPacket class instance.
        # Packet is either correct RFC791 packet or invalid. If packet is invalid then raise ParsingException with
        # human-readable message. If byte sequence is valid IP packet but header checksum does not maches actual
        # checksum, raise IntegrityException.
        if data[0] >> 4 != 4:
            raise ParsingException("invalid version value")
        if data[0] & 0b1111 < 5:
            raise ParsingException("invalid ihl value")
        if (data[2] << 8) | data[3] < 20 or (data[2] << 8) | data[3] > 65535:
            raise ParsingException("invalid total length size")
        return IPPacket(
            version=data[0] >> 4,
            ihl=data[0] & 0b1111,
            dscp=data[1] >> 2,
            ecn=data[1] & 0b11,
            total_length=(data[2] << 8) | data[3],
            identification=(data[4] << 8) | data[5],
            flags=Flags(data[6] >> 5),
            fragment_offset=(data[6] & 0b00011111) << 8 | data[7],
            ttl=data[8],
            protocol=data[9],
            header_checksum=(data[10] << 8) | data[11],
            source_ip=(data[12] << 24) | (data[13] << 16) | (data[14] << 8) | data[15],
            destination_ip=(data[16] << 24) | (data[17] << 16) | (data[18] << 8) | data[19],
            data=ProtocolPacket(ProtocolPacket(data).hash())
        )

    def to_bytes(self) -> bytes:
        ans = bytearray()
        octet0 = (self.version << 4 | self.ihl)
        ans.append(octet0)
        octet1 = (self.dscp << 2 | self.ecn)
        ans.append(octet1)
        octet2 = self.total_length >> 8
        ans.append(octet2)
        octet3 = self.total_length & 0b11111111
        ans.append(octet3)
        octet4 = self.identification >> 8
        ans.append(octet4)
        octet5 = self.identification & 0b11111111
        ans.append(octet5)
        octet6 = (self.flags.value << 5) | (self.fragment_offset >> 8)
        ans.append(octet6)
        octet7 = self.fragment_offset & 0b11111111
        ans.append(octet7)
        octet8 = self.ttl
        ans.append(octet8)
        octet9 = self.protocol
        ans.append(octet9)
        octet10 = self.header_checksum >> 8
        ans.append(octet10)
        octet11 = self.header_checksum & 0b11111111
        ans.append(octet11)
        octet12 = self.source_ip >> 24
        ans.append(octet12)
        octet13 = (self.source_ip >> 16) & 0b11111111
        ans.append(octet13)
        octet14 = (self.source_ip >> 8) & 0b11111111
        ans.append(octet14)
        octet15 = self.source_ip & 0b11111111
        ans.append(octet15)
        octet16 = self.destination_ip >> 24
        ans.append(octet16)
        octet17 = (self.destination_ip >> 16) & 0b11111111
        ans.append(octet17)
        octet18 = (self.destination_ip >> 8) & 0b11111111
        ans.append(octet18)
        octet19 = self.destination_ip & 0b11111111
        ans.append(octet19)
        last_octets = self.data.data
        ans += last_octets
        ans = bytes(ans)
        return ans