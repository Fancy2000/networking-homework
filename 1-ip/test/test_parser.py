import pytest
from parser import IPPacket, ProtocolPacket, Flags


def test_valid_serialization():
    ip_binary = (
        b"\x45\x00\x00\x34\xbe\x01\x40\x00\x37\x06\xde\x7c\xd9\x41\x0c\x18"
        b"\xc0\xa8\x01\x44"
    )

    tcp_binary = (
        b"\x01\xbb\xf9\xdd\xb9\x40\x7e\x64\x61\xe5\x87\x8c\x80\x10\x00\xf7"
        b"\xae\xf4\x00\x00\x01\x01\x08\x0a\x1f\xf2\x13\xf5\xac\xf5\x21\xfe"
    )

    full_packet = ip_binary + tcp_binary

    tcp_packet = ProtocolPacket(tcp_binary)

    packet_parsed = IPPacket(
        version=4,
        ihl=5,
        dscp=0,
        ecn=0,
        total_length=52,
        identification=0xBE01,
        flags=Flags(value=0x2),
        fragment_offset=0,
        ttl=55,
        protocol=6,
        header_checksum=0xDE7C,
        source_ip=0xD9410C18,
        destination_ip=0xC0A80144,
        data=tcp_packet,
    )

    assert IPPacket.from_bytes(full_packet) == packet_parsed
    assert IPPacket.from_bytes(full_packet).to_bytes() == full_packet