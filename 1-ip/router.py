import typing as t
from dslib import Context, Message, Node


class EndNode(Node):
    # DO NOT CHANGE THIS CLASS
    def __init__(self, node_ip, router_ip):
        self._ip = node_ip
        self._router_ip = router_ip

    def on_message(self, msg: Message, sender: str, ctx: Context):
        print(f"[EndNode] Got message from {sender}, sending to local")
        if msg["dst_addr"] == self._ip:
            ctx.send_local(msg)

    def on_local_message(self, msg: Message, ctx: Context):
        print(f"[EndNode] Got local message {msg}, sending to my router")
        ctx.send(msg, self._router_ip)


class Router(Node):
    def __init__(
            self,
            node_ip: str,
            node_mask: str,
            neighbors: t.Tuple[t.Tuple[str, str]],
            routing_table: t.Tuple[t.Tuple[str, str, str]],
    ):
        self._ip = node_ip
        self._node_mask = node_mask

        # Ip addresses of connected routers.
        # Feel free to change it to suit your needs
        self._neighbors = neighbors or []

        # On initialization consists of tuples (network_ip, network_mask, next_hop).
        # Feel free to change it to suit your needs
        self._routing_table = routing_table or []
        self._neighbors_ip = ""
        self.next = ""
        # Feel free to add your initialization code here


    def ip_to_int(self, ip: str):
        parts = ip.split('.')
        cur_ip = (int(parts[0]) << 24) + (int(parts[1]) << 16) + (int(parts[2]) << 8) + int(parts[3])
        return cur_ip

    def in_one_ip_range(self, str_network_range: str, msg_ip: str):
        for i in range(len(str_network_range)):
            if str_network_range[i] == "0":
                break
            if str_network_range[i] != msg_ip[i]:
                return False
        return True

    def same_network(self, msg: Message):
        cur_ip = self.ip_to_int(self._ip)
        cur_mask = self.ip_to_int(self._node_mask)
        network_range = cur_ip & cur_mask
        str_network_range = '.'.join([str(network_range >> (i << 3) & 0xFF) for i in range(4)[::-1]])
        msg_ip = msg["dst_addr"]
        ans = self.in_one_ip_range(str_network_range, msg_ip)
        return ans

    def across_networks(self, msg: Message):
        for neighbor in self._neighbors:
            cur_ip = self.ip_to_int(neighbor[0])
            cur_mask = self.ip_to_int(neighbor[1])
            network_range = cur_ip & cur_mask
            str_network_range = '.'.join([str(network_range >> (i << 3) & 0xFF) for i in range(4)[::-1]])
            msg_ip = msg["dst_addr"]
            if self.in_one_ip_range(str_network_range, msg_ip):
                self._neighbors_ip = neighbor[0]
                return True
        return False

    def through_network(self, msg: Message):
        for elem in self._routing_table:
            cur_ip = self.ip_to_int(elem[0])
            cur_mask = self.ip_to_int(elem[1])
            network_range = cur_ip & cur_mask
            str_network_range = '.'.join([str(network_range >> (i << 3) & 0xFF) for i in range(4)[::-1]])
            msg_ip = msg["dst_addr"]
            if self.in_one_ip_range(str_network_range, msg_ip):
                self.next = elem[2]
                return True
        return False



    def on_message(self, msg: Message, sender: str, ctx: Context):
        # TODO: Process messages from router and nodes
        if self.same_network(msg):
            ctx.send_local(msg)
        elif self.across_networks(msg):
            ctx.send(msg, self._neighbors_ip)
        elif self.through_network(msg):
            ctx.send(msg, self.next)



    def on_timer(self, timer_id: str, ctx: Context):
        pass

