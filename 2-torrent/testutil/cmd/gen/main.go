package main

import (
	"flag"
	"log"
	"testutil/prepenv"
)

func main() {
	var (
		confFile string
		dataFile string
		dataSize int
		partSize int
	)

	flag.StringVar(&confFile, "conf", "", "config file")
	flag.StringVar(&dataFile, "data", "", "data file")
	flag.IntVar(&dataSize, "size", 1024*1024, "data size")
	flag.IntVar(&partSize, "part", 1024, "part size")

	flag.Parse()

	if confFile == "" || dataFile == "" {
		flag.Usage()
		return
	}

	log.Println(prepenv.GenerateFullFiles(dataFile, confFile, dataSize, partSize, []string{}))
}
