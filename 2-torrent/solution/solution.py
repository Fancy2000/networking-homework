import sys
import os
import asyncio
import struct
from config import *
from utils import *
from dataclasses import dataclass


want_parts: set[int] = set()
config = read_config()
file_storage = FileStorage(config.file_info)
have_connection = set()

global_dead_pears = [[], []]

@dataclass
class Message:
    mtype: int
    data: bytes


# conn_lock это блокировка для соединения, она нужна чтобы мы не вызывали writer.drain() одновременно
# из разных корутин (это приводит к AssertionError)
async def write_message(conn_lock: asyncio.Lock, writer: asyncio.StreamWriter, message: Message) -> None:
    await conn_lock.acquire()
    try:
        if writer.is_closing():
            return
        writer.write(struct.pack('LL', message.mtype, len(message.data)))
        writer.write(message.data)
        await writer.drain()
    except Exception as e:
        print(e)
    finally:
        conn_lock.release()


async def read_message(reader: asyncio.StreamReader, message: Message) -> None:
    buf = await reader.readexactly(16)
    message.mtype, length = struct.unpack('LL', buf)
    message.data = await reader.readexactly(length)


async def handle_socket(reader: asyncio.StreamReader, writer: asyncio.StreamWriter) -> None:
    lock = asyncio.Lock()
    try:
        while True:
            msg = Message(0, b'')
            try:
                await read_message(reader, msg)
            except:
                break
            parts = config.file_info.parts.index(msg.data.decode('utf-8'))
            print("take part:", parts, flush=True)
            if parts in want_parts:
                await write_message(lock, writer, Message(0, b''))
            else:
                data = await verify_piece(file_storage, parts, config.file_info.parts[parts])
                await write_message(lock, writer, Message(0, data))
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e, flush=True)
    writer.close()
    await writer.wait_closed()
