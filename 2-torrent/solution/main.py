import socket
from config import *
from solution import want_parts, asyncio, handle_socket, config, verify_piece, file_storage
from solution import have_connection, Message, write_message, read_message, global_dead_pears

port = 8888


async def main() -> None:
    server = await asyncio.start_server(handle_socket, '0.0.0.0', port)
    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    print(f'Serving on {addrs}', flush=True)
    await asyncio.gather(
        server.serve_forever(),
        background_routine(),
    )


async def need_parts():
    want_parts.clear()
    if config.file_info.parts is None:
        return set()
    for i in range(len(config.file_info.parts)):
        if await verify_piece(file_storage, i, config.file_info.parts[i]) is None:
            want_parts.add(i)
    return want_parts


async def background_routine() -> None:
    parts_we_need = await need_parts()
    print("want_parts:", parts_we_need, flush=True)
    if config.peers is None:
        config.peers = set()
    if config.file_info is None:
        config.file_info = Optional[FileInfo]
    global_dead_pears[0].append(socket.gethostbyname(socket.gethostname()))
    global_dead_pears[1].append(config)
    available_peers = set(config.peers) - {socket.gethostbyname(socket.gethostname())}
    for i in global_dead_pears[0]:
        available_peers.add(i)
    lock = asyncio.Lock()
    while len(parts_we_need):
        count = 0
        for peer in available_peers:
            if peer not in have_connection:
                try:
                    reader, writer = await asyncio.open_connection(peer, port)
                except:
                    continue
                have_connection.add(peer)
            else:
                continue
            taken_parts = set()
            for part in parts_we_need:
                await write_message(lock, writer, Message(0, config.file_info.parts[part].encode('utf-8')))
                msg = Message(0, b'')
                try:
                    await asyncio.wait_for(read_message(reader, msg), timeout=1)
                except:
                    continue
                if msg.data == b'':
                    continue
                await file_storage.write_block(part, msg.data)
                taken_parts.add(part)
            print("taken parts:", taken_parts, flush=True)
            parts_we_need -= taken_parts
            print("left parts to take", parts_we_need, flush=True)
            count += 1
            if count == 6:
                count = 0
                await asyncio.sleep(2)

        for index, peer in enumerate(global_dead_pears[0]):
            if peer not in have_connection:
                try:
                    reader, writer = await asyncio.open_connection(peer, port)
                except:
                    continue
                have_connection.add(peer)
            else:
                continue
            taken_parts = set()
            for part in parts_we_need:
                await write_message(lock, writer, Message(0, global_dead_pears[1][index].file_info.parts[part].encode('utf-8')))
                msg = Message(0, b'')
                try:
                    await asyncio.wait_for(read_message(reader, msg), timeout=1)
                except:
                    continue
                if msg.data == b'':
                    continue
                await file_storage.write_block(part, msg.data)
                taken_parts.add(part)
            print("taken parts: from probably dead peer", taken_parts, flush=True)
            parts_we_need -= taken_parts
            print("left parts to take", parts_we_need, flush=True)
            count += 1
            if count == 6:
                count = 0
                await asyncio.sleep(2)

asyncio.run(main())
